package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import LSys;
	
	/**
	 * ...
	 * @author tf
	 */
	public class Main extends Sprite 
	{
		
		protected var l:LSys2;
		
		public function Main():void 
		{
			l = new LSys2()
			l.x = 300
			l.y = 300
			addChild(l)
			
			stage.addEventListener(MouseEvent.CLICK, onClick)
		}
		
		protected function onClick(e:MouseEvent):void {
			removeChild(l)
			l = null
			l = new LSys2()
			l.x = 300
			l.y = 300
			addChild(l)
		}
		
	}
	
}